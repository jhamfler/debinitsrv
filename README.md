First run this as root:

https://gitlab.com/jhamfler/debinitclient/blob/master/prerootinit.sh#L1-L3
```
# "ich" kann bearbeitet werden, keine Leerzeichen erlaubt
ich=jh
```

```
gruppe=sudo
install=

# lege Paketmanager fest
if [ "$(grep -io debian /proc/version)" ]
then
        echo Debian
        apt-get update
        apt-get upgrade -y
        install="apt-get install -y "
fi
if [ "$(grep -io arch /proc/version)" ]
then
        echo Arch
        pacman -Syu
        install="pacman -Sy "
fi

if [ -z "$install" ]
then
        echo Distribution nicht erkannt
        exit
fi

# installiere und lege sudo fest
$install sudo git
useradd -m $ich

if [ "$(grep -i sudo /etc/group)" ]
then
        gruppe=sudo
else
        if [ "$(grep -i wheel /etc/group)" ]
        then
                gruppe=wheel
        fi
fi
usermod -aG $gruppe $ich
```

Then run this.
```
sudo -u $ich bash -i
```

https://gitlab.com/jhamfler/debinitclient/blob/master/preinit.sh


```
cd
git config --global user.name "JH"
git config --global user.email "a@bc.de"
mkdir git
cd git
git clone https://gitlab.com/jhamfler/debinitsrv.git
cd debinitsrv
./init.sh
```

```
exit
```

```
exit
```

```
reboot
```
