#!/bin/bash
ich=jh

# lege Paketmanager fest
if [ "$(grep -io debian /proc/version)" ]
then
        echo Debian
        install="sudo apt-get install -y "
	$install p7zip-full
fi
if [ "$(grep -io arch /proc/version)" ]
then
        echo Arch
	# schnellste mirrors finden
        sudo pacman -Sy reflector
	sudo reflector --verbose --country Germany -c Italy -c France -c Denmark -c Switzerland -c Autria -l 400 -p 
https --sort rate --save /etc/pacman.d/mirrorlist
        install="sudo pacman -Sy "
	$install p7zip
fi
if [ "$(grep -io raspbian /etc/os-release)" ]
then
        echo Raspbian
        install="sudo apt-get install -y "
        $install p7zip-full
fi
if [ -z "$install" ]
then
        echo Distribution nicht erkannt
        exit
fi

$install htop byobu locate nmap
$install wget curl zsh
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh) && exit"
wait
sudo chsh -s $(which zsh) $ich

cd
cd git
git clone https://github.com/jhamfler/debconfsrv.git
cd debconfsrv
./apply.sh
echo "debconfsrv applied"

if [[ $# -gt 0 ]]; then
  if [[ "$1" == "pi" ]] ; then
    cd
    cd git
    git clone https://gitlab.com/jhamfler/debinitsrvpi.git
    cd debinitsrvpi
    ./init.sh
  fi
fi

#sudo reboot
