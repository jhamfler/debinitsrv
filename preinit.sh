#!/bin/bash
git config --global user.name "Johannes Hamfler"
git config --global user.email "jh@z7k.de"
mkdir git
cd git
git clone https://gitlab.com/jhamfler/debinitclient.git
cd debinitclient
./init.sh
